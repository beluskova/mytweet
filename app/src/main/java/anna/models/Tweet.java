package anna.models;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import annab.mytweetActivity.R;

public class Tweet
{
    public String id;
    public String  editStatus;
    public Date    datestamp;
    public String  selectContact;

    private static final String JSON_ID             = "id"            ;
    private static final String JSON_EDITSTATUS    = "editStatus"   ;
    private static final String JSON_DATE           = "date"          ;
    private static final String JSON_CONTACT        = "selectContact";

    public Tweet()
    {
        this.id = UUID.randomUUID().toString();
        this.datestamp = new Date();

        this.editStatus = "";
        selectContact = "no contact";
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id           = json.getString(JSON_ID);
        editStatus   = json.getString(JSON_EDITSTATUS);
        datestamp          = new Date(json.getLong(JSON_DATE));
        selectContact        = json.getString(JSON_CONTACT);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , id.toString());
        json.put(JSON_EDITSTATUS    , editStatus);
        json.put(JSON_DATE          , datestamp.getTime());
        json.put(JSON_CONTACT       , selectContact);
        return json;
    }

    public void editTweet(String editStatus)
    {
        this.editStatus = editStatus;
    }

 /*   public String getDateString()
    {
        return DateFormat.getDateTimeInstance().format(date);
    }*/

    /**
     * Formats the datestamp field by first converting
     * to a Date object and then using the private
     * helper method formattedDate.
     */
    public String getDateString()
    {
        return formattedDate(new Date());
    }

    /**
     * @param date A java.util.Date object
     * @see <a href="http://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html">Date Format</a>
     * @see <a href="http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html">Simple Date Format</a>
     * @return A formatted date string
     */
    private String formattedDate(Date date)
    {
        DateFormat sdf = new SimpleDateFormat("EEE d MMM yyyy H:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(datestamp);
    }

    public String getTweetEmailed(Context context)
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, datestamp).toString();
        String report =  "Tweet " + editStatus + " Date: " + dateString + " ";
        return report;
    }

    public String getTweet(Context context)
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, datestamp).toString();
        String friendContact = selectContact;

        if (selectContact == null)
        {
            friendContact = context.getString(R.string.no_contacts);
        }
        else
        {
            friendContact = context.getString(R.string.contact, selectContact);
        }
        //String report =  "text: " + message_text + " Date: " + dateString;
        String report =  editStatus;
        return report;
    }
}

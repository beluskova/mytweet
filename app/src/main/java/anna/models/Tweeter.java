package anna.models;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit.Call;

public class Tweeter {
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;

    public ArrayList<Tweet> tweets = new ArrayList<Tweet>();
    public List<Friendship> friendships = new ArrayList<Friendship>();

    public Tweeter(String firstName, String lastName, String email, String password) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
      }

    public void befriend(Tweeter friend) {
        Friendship friendship = new Friendship(this, friend);
        friendships.add(friendship);
        friendship.save();
    }

    public void unfriend(Tweeter friend) {
        Friendship thisFriendship = null;

        for (Friendship friendship : friendships) {
            if (friendship.targetUser == friend) {
                thisFriendship = friendship;
            }
        }

        friendships.remove(thisFriendship);
    }
}

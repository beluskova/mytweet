package anna.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.util.Log;

import static anna.android.helpers.LogHelpers.info;

public class Timeline

{
    public  ArrayList<Tweet> tweets;
    private TimelineSerializer   serializer;
    public  ArrayList<Tweeter> tweeters;
    public ArrayList<Tweeter>friends;

    // timeline is refactored so it includes the serializer now to make sure the tweets are saved when we leave the app
    public Timeline(TimelineSerializer serializer)
    {
        this.serializer = serializer;
        try {
            tweets = serializer.loadMessages();
        }
        catch (Exception e)
        {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }
    //tweets saved through the serializer
    public boolean saveMessages()
    {
        try
        {
            serializer.saveMessages(tweets);
            info(this, "Messages saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    //tweets saved through the setializer
  /*  public boolean saveTweeters)
    {
        try
        {
            serializer.saveTweeters(tweeters);
            info(this, "Tweeter saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }*/
    public void addMessage(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public Tweet getTweet(String id)
    {
        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweet tweet : tweets)
        {
            if(id.equals(tweet.id))
            {
                return tweet;
            }
        }
        info(this, "failed to find message. returning first element array to avoid crash");
        return null;
    }

    private void generateTestData()
    {
        for(int i = 0; i < 3; i += 1)
        {
            Tweet m = new Tweet();
            m.editStatus = "I'm just saying hello";
            tweets.add(m);
        }
    }

    //to delete all tweets
    public void deleteAllMessages()
    {
        tweets = new ArrayList<Tweet>();
    }

    //to delete a single tweet
    public void deleteMessage(Tweet m)
    {
        tweets.remove(m);
    }

    public void updateTweets (List<Tweet> list)
    {
        tweets.clear();
        tweets.addAll(list);
        saveMessages();
    }

    public void updateTweeters (List<Tweeter> tweeters)
    {
        friends.clear();
        friends.addAll(tweeters);
       // saveMessages();
    }
}
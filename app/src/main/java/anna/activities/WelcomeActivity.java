package anna.activities;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import anna.app.MyTweetApp;
import anna.models.Tweeter;
import annab.mytweetActivity.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class WelcomeActivity extends Activity implements Callback<List<Tweeter>>
{
    private MyTweetApp app;
    public ArrayList<Tweeter> tweeters;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        app = (MyTweetApp) getApplication();

    }


    @Override
    public void onResume()
    {
        super.onResume();
        app.currentTweeter = null;
        Call<List<Tweeter>> call = app.mytweetService.getAllTweeters();
        call.enqueue(this);

    }

    @Override
    public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit) {
        app.tweeters = response.body();
        int numberTweeters = app.tweeters.size();
        app.mytweetServiceAvailable = true;
        serviceAvailableMessage();
        Toast.makeText(this, "Retrieved " + numberTweeters + " tweeters", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(this, "Failed to retrieve tweeter list", Toast.LENGTH_LONG).show();
    }

    public void loginPressed (View view)
    {
        if (app.mytweetServiceAvailable)
        {
            startActivity (new Intent(this, LoginActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void signupPressed (View view)
    {
        if (app.mytweetServiceAvailable)
        {
            startActivity (new Intent(this, SignupActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }
}



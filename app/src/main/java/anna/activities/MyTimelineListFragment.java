package anna.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import anna.android.helpers.IntentHelper;
import anna.app.MyTweetApp;
import anna.models.Timeline;
import anna.models.Tweet;
import anna.models.Tweeter;
import annab.mytweetActivity.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class MyTimelineListFragment extends ListFragment
        implements AdapterView.OnItemClickListener,
        AbsListView.MultiChoiceModeListener,
        Callback<List<Tweet>>
{
    public ArrayList<Tweet> tweets;
    public Timeline timeline;
    private MessageAdapter adapter;
    private ListView listView;
    private Tweeter tweeter;
    public MyTweetApp app;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.my_timeline);

        app = (MyTweetApp) getActivity().getApplication();
        tweeter   = app.logged_in_tweeter;
        timeline = app.timeline;
        tweets = tweeter.tweets;

        adapter = new MessageAdapter(getActivity(), tweeter.tweets);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Tweet mes = ((MessageAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), MytweetPagerActivity.class);
        i.putExtra(TweetFragment.EXTRA_MESSAGE_ID, mes.id);
        startActivityForResult(i, 0);
    }
    //saving new tweets
    @Override
    public void onResume()
    {
        super.onResume();
        ((MessageAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_mytimeline, menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case R.id.action_refresh:
                refreshMyTweetList();
                return true;
            case R.id.action_timeline :
                IntentHelper.startActivity(getActivity(), TimelineListActivity.class);
                break;
            case R.id.action_settings :
                IntentHelper.startActivity(getActivity(), SettingsActivity.class);
                break;
            case R.id.new_tweet:
                Tweet tweet = new Tweet();
                timeline.addMessage(tweet);
                Intent i = new Intent(getActivity(), MytweetPagerActivity.class);
                i.putExtra(TweetFragment.EXTRA_MESSAGE_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;
                default:
                return super.onOptionsItemSelected(item);
            case R.id.action_clear :
                deleteAllRemoteTweets();
                IntentHelper.startActivity(getActivity(), TimelineListActivity.class);
                //timeline.deleteAllMessages();
                break;
        }
        return true;
    }

    private void refreshMyTweetList() {

        Call<List<Tweet>> call = app.mytweetService.getTweets(app.logged_in_tweeter.id);
        call.enqueue(new Callback<List<Tweet>>() {

            @Override
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                List<Tweet> list = response.body();
                app.timeline.updateTweets(list);
                ((MessageAdapter) getListAdapter()).notifyDataSetChanged();
                //adapter.notifyDataSetChanged();
                startActivity(new Intent(getActivity(), TimelineListActivity.class));
                Toast.makeText(getActivity(), "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to Refresh Tweet List", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteRemoteTweet(String id, String tweetId)
    {
        Call<Tweet> call = app.mytweetService.deleteTweet(id, tweetId);
        call.enqueue(new Callback<Tweet>() {

            @Override
            public void onResponse(Response<Tweet> response, Retrofit retrofit) {
                Toast.makeText(getActivity(), "Tweet has been deleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to delete selected tweet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteAllRemoteTweets ()
    {
        timeline.deleteAllMessages();
        Call<String> call = app.mytweetService.deleteAllTweets();
        call.enqueue(new Callback<String>()
        {

            @Override
            public void onResponse(Response<String> response, Retrofit retrofit)
            {
                Toast.makeText(getActivity(), "All Tweets deleted: "+response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t)
            {
                Toast.makeText(getActivity(), "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //to edit an individual tweet
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), MytweetPagerActivity.class, "MESSAGE_ID", tweet.id);
    }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.message_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_delete_message:
                 deleteMessage(mode);
                return true;
            default:
                return false;
        }
    }

    private void deleteMessage(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--)
        {
            if (listView.isItemChecked(i))
            {
                Tweet tweet = adapter.getItem(i);
                timeline.deleteMessage(tweet);
                deleteRemoteTweet(tweeter.id, tweet.id);
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }

    @Override
    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {

    }

    @Override
    public void onFailure(Throwable t) {

    }
    /* ************ MultiChoiceModeListener methods (end) *********** */

class MessageAdapter extends ArrayAdapter<Tweet> {
    private Context context;
    public List<Tweet> tweets;

    public MessageAdapter(Context context, List<Tweet> tweets) {
        super(context, R.layout.list_item_message, tweets);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_message, null);
        }

        Tweet mes = getItem(position);

        TextView editStatus = (TextView) convertView.findViewById(R.id.list_item_tweet_message);
        editStatus.setText(mes.editStatus);

        TextView dateTextView = (TextView) convertView.findViewById(R.id.list_item_dateTextView);
        dateTextView.setText(mes.getDateString());
        return convertView;
    }
}
}



//a new class to support swipe activity
package anna.activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.UUID;

import anna.app.MyTweetApp;
import anna.models.Tweet;
import anna.models.Timeline;
import annab.mytweetActivity.R;

import static anna.android.helpers.LogHelpers.info;


public class MytweetPagerActivity extends FragmentActivity  implements ViewPager.OnPageChangeListener {
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets;
    private Timeline timeline;
    private PagerAdapter pagerAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.mytweet_activity);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);
        setCurrentItem();
    }

    private void setTweetList() {
        MyTweetApp app = (MyTweetApp) getApplication();
        timeline = app.timeline;
        tweets = timeline.tweets;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        info(this, "onPageScrolled: arg0 " + arg0 + " arg1 " + arg1 + " arg2 " + arg2);
        Tweet tweet = tweets.get(arg0);
        if (tweet.editStatus != null) {
            setTitle(tweet.editStatus);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    //Ensure selected residence is shown in details view
    private void setCurrentItem() {
        String res = (String) getIntent().getSerializableExtra(TweetFragment.EXTRA_MESSAGE_ID);
        for (int i = 0; i < tweets.size(); i++) {
            if (tweets.get(i).id.toString().equals(res.toString())) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Tweet> tweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets) {
            super(fm);
            this.tweets = tweets;
        }

        @Override
        public int getCount() {
            return tweets.size();
        }

        @Override
        public Fragment getItem(int pos) {
            Tweet tweet = tweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(TweetFragment.EXTRA_MESSAGE_ID, tweet.id);
            TweetFragment fragment = new TweetFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }
}



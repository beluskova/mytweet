package anna.activities;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import anna.app.MyTweetApp;
import annab.mytweetActivity.R;


public class LoginActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void signinPressed(View view) {
         MyTweetApp app = (MyTweetApp) getApplication();

        TextView email = (TextView) findViewById(R.id.loginEmail);
        TextView password = (TextView) findViewById(R.id.loginPassword);

        if (app.validTweeter(email.getText().toString(), password.getText().toString()))
            startActivity(new Intent(this, TimelineListActivity.class));
        else {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            startActivity(new Intent(this, WelcomeActivity.class));
            toast.show();
        }
    }
}
package anna.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.UUID;

import anna.android.helpers.ContactHelper;
import anna.android.helpers.IntentHelper;
import anna.app.MyTweetApp;
import anna.models.Tweet;
import anna.models.Timeline;
import anna.models.Tweeter;
import annab.mytweetActivity.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


import static android.provider.ContactsContract.Contacts.CONTENT_URI;
import static anna.android.helpers.IntentHelper.navigateUp;

public class TweetFragment extends Fragment implements View.OnClickListener, TextWatcher, Callback<Tweet> {

    public static   final String  EXTRA_MESSAGE_ID = "mytweet.MESSAGE_ID";
    private static final int REQUEST_CONTACT = 1;

    private Button buttonTweet;
    private TextView textCount;
    private EditText editStatus;
    private TextView sent_date;
    private Button selectContact;
    private Button sendEmail;
    private Tweet tweet;
    private Timeline timeline;
    private MyTweetApp app;
    private Tweeter tweeter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String tweetId = (String)getArguments().getSerializable(EXTRA_MESSAGE_ID);

        app = (MyTweetApp) getActivity().getApplication();
        timeline = app.timeline;
        tweet = timeline.getTweet(tweetId);
        tweeter = app.logged_in_tweeter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_message, parent, false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }

    private void addListeners(View v) {
        buttonTweet = (Button) v.findViewById(R.id.buttonTweet);
        textCount = (TextView) v.findViewById(R.id.textCount);
        editStatus = (EditText) v.findViewById(R.id.editStatus);
        sent_date = (TextView) v.findViewById(R.id.sent_date);
        selectContact = (Button) v.findViewById(R.id.selectContact);
        sendEmail = (Button) v.findViewById(R.id.sendEmail);

        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a");
        String dateString = sdf.format(date);
        sent_date.setText(dateString);
        buttonTweet.setOnClickListener(this);
        editStatus.addTextChangedListener(this);
        selectContact.setOnClickListener(this);
        sendEmail.setOnClickListener(this);

    }

    public void updateControls(Tweet tweet)
    {
        editStatus.setText(tweet.editStatus);
        sent_date.setText(tweet.getDateString());
    }

    public void afterTextChanged (Editable s)
    {
        int count = 140 - editStatus.length();
        textCount.setText(Integer.toString(count));
        textCount.setTextColor(Color.BLACK);
        if (count < 10 & count > 0)
            textCount.setTextColor(Color.YELLOW);
        else if (count == 0)
            textCount.setTextColor(Color.RED);
        else
            textCount.setTextColor(Color.BLACK);

        String thisMessage = s.toString();
        Log.i(this.getClass().getSimpleName(), "tweeted: " + thisMessage);
        tweet.editStatus = thisMessage;
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a");
        String dateString = sdf.format(date);
        sent_date.setText(dateString);
        tweet.editTweet(s.toString());

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(getActivity());
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    // @Override
    public void onClick (View v) {
        switch  (v.getId())
        {
            case R.id.buttonTweet:
                Toast toast = Toast.makeText(getActivity(), "Tweet Sent ", Toast.LENGTH_SHORT);
                toast.show();
                //to create tweet remotely in cloud
                createRemoteTweet();
                //to return to Timeline when tweet is sent: no more editing of tweet is prevented
                IntentHelper.startActivity(getActivity(), TimelineListActivity.class);
                break;

            case R.id.selectContact:
                Intent i = new Intent(Intent.ACTION_PICK, CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.selectContact != null)
                {
                    selectContact.setText( tweet.selectContact);
                }
                break;

            case R.id.sendEmail:
                IntentHelper.sendEmail(getActivity(), String.valueOf(selectContact.getText()), getString(R.string.tweet_report_subject), tweet.getTweetEmailed(getActivity()));
                break;
        }
    }
    //to make sure the tweets are saved when we leave the app
    public void onPause()
    {
        super.onPause();
        timeline.saveMessages();
    }

    //a helping method to display selected person's email when Select Contact button is clicked
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getEmail(getActivity(), data);
            tweet.selectContact = name;
            selectContact.setText(name);
        }
    }

    @Override
    public void onResponse(Response<Tweet> response, Retrofit retrofit)
    {
        Tweet returnedTweet = response.body();
        // If returned tweet matches the tweet we created and transmitted to the server then success.
         if (!returnedTweet.id.equals(tweet.id)) {
            Toast.makeText(getActivity(), "Incorrect transmission tweet", Toast.LENGTH_LONG).show();
            editStatus.setText("");
        }
        else
        {
            Toast.makeText(getActivity(), "Successfully created Tweet on service", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(getActivity(), "Failed to create tweet due to unknown network issue", Toast.LENGTH_LONG).show();
    }

    private void createRemoteTweet()
    {
             // Make the API call once the tweet has been fully formed.
            //Call<Tweet> call = app.mytweetService.createTweet(tweet.id, tweet);
            app.newTweet(tweet);
            Call<Tweet> call = app.mytweetService.createTweet(app.logged_in_tweeter.id, tweet);
            call.enqueue(this);
    }
}
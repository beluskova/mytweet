package anna.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import anna.app.MyTweetApp;
import anna.models.Tweeter;
import annab.mytweetActivity.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SignupActivity extends Activity implements Callback<Tweeter>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
    }

    public void registerPressed (View view)
    {
        TextView firstName = (TextView)  findViewById(R.id.firstName);
        TextView lastName  = (TextView)  findViewById(R.id.lastName);
        TextView email     = (TextView)  findViewById(R.id.Email);
        TextView password  = (TextView)  findViewById(R.id.Password);

       Tweeter tweeter = new Tweeter(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

        //app = (MyTweetApp) getApplication();
        Call<Tweeter> call = app.mytweetService.createTweeter(tweeter);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<Tweeter> response, Retrofit retrofit) {
        app.tweeters.add(response.body());
        startActivity(new Intent(this, TimelineListActivity.class));
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.mytweetServiceAvailable = false;
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
        startActivity (new Intent(this, WelcomeActivity.class));
    }
}
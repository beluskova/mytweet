package anna.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import anna.android.helpers.IntentHelper;
import anna.app.MyTweetApp;
import anna.models.Friendship;
import anna.models.Timeline;
import anna.models.Tweet;
import anna.models.Tweeter;
import annab.mytweetActivity.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class FriendsListFragment extends ListFragment
        implements AdapterView.OnItemClickListener,
        AbsListView.MultiChoiceModeListener,
        Call<Tweeter> {
    public ArrayList<Tweet> tweets;
    public ArrayList<Tweeter>tweeters;
    private ArrayList<Tweeter> friends;
    public Timeline timeline;
    private TweeterAdapter adapter;
    private ListView listView;
    private Tweeter tweeter;
    public MyTweetApp app;
    private IntentFilter intentFilter;
    private Friendship friendship;
    public static final String BROADCAST_ACTION = "anna.activities.TimelineListFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.title_activity_members);

        app = (MyTweetApp) getActivity().getApplication();
        tweeter = app.logged_in_tweeter;
        timeline = app.timeline;
        tweets = timeline.tweets;
       // friends = tweeter.friends;


        adapter = new TweeterAdapter(getActivity(), friends);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();

        // intentFilter = new IntentFilter(BROADCAST_ACTION);
        //registerBroadcastReceiver(intentFilter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    //saving new tweets
    @Override
    public void onResume() {
        super.onResume();
        app.currentTweeter = app.logged_in_tweeter;
        Call<Tweeter> friends = app.mytweetService.getTweeter(app.logged_in_tweeter.id);
        friends.enqueue((Callback<Tweeter>) this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_timeline, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshFriendsList();
                return true;
            case R.id.action_members :
                Intent members = new Intent(getActivity(), MemberListActivity.class);
                startActivity(members);
                break;
            case R.id.action_mytimeline:
                Intent in = new Intent(getActivity(), MyTimelineListActivity.class);
                startActivity(in);
                break;
            case R.id.action_settings:
                IntentHelper.startActivity(getActivity(), SettingsActivity.class);
                break;
            case R.id.new_tweet:
                Tweet tweet = new Tweet();
                timeline.addMessage(tweet);
                Intent i = new Intent(getActivity(), MytweetPagerActivity.class);
                i.putExtra(TweetFragment.EXTRA_MESSAGE_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
            case R.id.action_clear:
                deleteAllRemoteTweets();
                IntentHelper.startActivity(getActivity(), TimelineListActivity.class);
                //timeline.deleteAllMessages();
                break;
        }
        return true;
    }

    private void refreshFriendsList() {
        Call<Tweeter> friends = app.mytweetService.getTweeter(app.logged_in_tweeter.id);
        friends.enqueue(new Callback<Tweeter> ()
        {

            @Override
            public void onResponse(Response<Tweeter> response, Retrofit retrofit) {
                List<Tweeter> friends = (List<Tweeter>) response.body();
                app.timeline.updateTweeters(friends);
                ((TweeterAdapter) getListAdapter()).notifyDataSetChanged();
                //adapter.notifyDataSetChanged();
                startActivity(new Intent(getActivity(), FriendsListActivity.class));
                Toast.makeText(getActivity(), "Retrieved " + friends.size() + "friends", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to Refresh Tweet List", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteRemoteTweet(String id, String tweetId) {
        Call<Tweet> call = app.mytweetService.deleteTweet(id, tweetId);
        call.enqueue(new Callback<Tweet>() {

            @Override
            public void onResponse(Response<Tweet> response, Retrofit retrofit) {
                Toast.makeText(getActivity(), "Tweet has been deleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to delete selected tweet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteAllRemoteTweets() {
        timeline.deleteAllMessages();
        Call<String> call = app.mytweetService.deleteAllTweets();
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                Toast.makeText(getActivity(), "All Tweets deleted: " + response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /*   private void registerBroadcastReceiver(IntentFilter intentFilter)
       {
           ResponseReceiver responseReceiver = new ResponseReceiver();
           // Registers the ResponseReceiver and its intent filters
           LocalBroadcastManager.getInstance(getActivity()).registerReceiver(responseReceiver, intentFilter);
       }

       //Broadcast receiver for receiving status updates from the IntentService
       private class ResponseReceiver extends BroadcastReceiver {
           //private void ResponseReceiver() {}
           // Called when the BroadcastReceiver gets an Intent it's registered to receive
           @Override
           public void onReceive(Context context, Intent intent) {
               refreshTweetList();
               adapter.tweeters = app.tweeters;
               adapter.notifyDataSetChanged();
           }
       }  */
    //to edit an individual tweet
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tweeter tweeter = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), MytweetPagerActivity.class, "TWEETER_ID", tweeter.id);
    }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.message_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_follow_tweeter:
                followTweeter(mode);
                return true;
            default:
                return false;
        }
    }

    private void followTweeter(ActionMode actionMode)
       {
           for (int i = adapter.getCount() - 1; i >= 0; i--)
           {
               if (listView.isItemChecked(i))
               {
                   Tweeter tweeter = adapter.getItem(i);

                   app.newFriendship(friendship);
                   //timeline.deleteMessage(tweet);
                  // deleteRemoteTweet(tweeter.id, tweet.id);
               }
           }
           actionMode.finish();
           adapter.notifyDataSetChanged();
       }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }



    @Override
    public Response<Tweeter> execute() throws IOException {
        return null;
    }

    @Override
    public void enqueue(Callback<Tweeter> callback) {

    }

    @Override
    public void cancel() {

    }

    @Override
    public Call<Tweeter> clone() {
        return null;
    }

    /* ************ MultiChoiceModeListener methods (end) *********** */

    class TweeterAdapter extends ArrayAdapter<Tweeter>
    {
        private Context context;
        public List<Tweeter> friends;

        public TweeterAdapter(Context context, List<Tweeter> friends) {
            super(context, R.layout.list_item_tweeter, friends);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_tweeter, null);
            }

            Tweeter tweeter = getItem(position);

            TextView firstName = (TextView) convertView.findViewById(R.id.list_item_tweeter_firstName);
            firstName.setText(tweeter.firstName);
            TextView lastName = (TextView) convertView.findViewById(R.id.list_item_tweeter_lastName);
            lastName.setText(tweeter.lastName);
            return convertView;
        }
    }
}



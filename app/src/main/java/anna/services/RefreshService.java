package anna.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.IOException;
import java.util.List;

import anna.activities.TimelineListFragment;
import anna.android.helpers.LogHelpers;
import anna.app.MyTweetApp;
import anna.models.Tweet;
import retrofit.Call;
import retrofit.Response;

public class RefreshService extends IntentService
{
    private String tag = "MyTweet";
    MyTweetApp app;

    public RefreshService()
    {
        super("RefreshService");

    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        app = (MyTweetApp) getApplication();
        Intent localIntent = new Intent(TimelineListFragment.BROADCAST_ACTION);
        Call<List<Tweet>> call = (Call<List<Tweet>>) app.mytweetService.getAllTweets();
        try
        {
            Response<List<Tweet>> response = call.execute();
            app.tweets = response.body();
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
        catch (IOException e)
        {
            LogHelpers.info(tag, "Failed to retrieve tweets list - network error");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        LogHelpers.info(this, "onDestroyed");
    }
}
package anna.app;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import anna.main.MyTweetServiceProxy;
import anna.models.Friendship;
import anna.models.Timeline;
import anna.models.TimelineSerializer;
import anna.models.Tweet;
import anna.models.Tweeter;
import anna.services.RefreshService;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

import static anna.android.helpers.LogHelpers.info;

public class MyTweetApp extends Application
{
    //adding new file where tweets are saved to
    private static final String FILENAME = "portfolio.json";
    public Timeline timeline;

    public String  service_url  = "http://10.0.2.2:9000";   // Standard Emulator IP Address
   // public String  service_url = "http://10.0.3.2:9000"; //Genymotion
   //public String          service_url  = "http://mytweetbyannab.herokuapp.com";

    public MyTweetServiceProxy  mytweetService;
    public boolean              mytweetServiceAvailable = true;

    public Tweeter               currentTweeter;

    public List<Tweeter> tweeters = new ArrayList<Tweeter>();;
    public List<Tweet> tweets = new ArrayList<Tweet>();;
    public List<Friendship> friendships = new ArrayList<Friendship>();
    public Tweeter logged_in_tweeter;
    private Timer timer;


    @Override
    public void onCreate()
    {
        super.onCreate();
        //adding serializer to make sure the tweets are saved when we leave the app
        TimelineSerializer serializer = new TimelineSerializer(this, FILENAME);
        timeline = new Timeline(serializer);
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mytweetService = retrofit.create(MyTweetServiceProxy.class);

        timer = new Timer();
        //refreshTimelineList();

        info(this, "MyTweet app launched");
    }

    public boolean validTweeter (String email, String password)
    {
        for (Tweeter tweeter : tweeters)
        {
            if (tweeter.email.equals(email) && tweeter.password.equals(password))
            {
                logged_in_tweeter = tweeter;
                return true;
            }
        }
        return false;
    }

    public void newFriendship(Friendship friendship)
    {
        friendships.add(friendship);
    }

    public void newTweet(Tweet tweet)
        {
            tweets.add(tweet);
        }

    public void newTweeter(Tweeter tweeter)
        {
           tweeters.add(tweeter);
        }

    public void refreshTimelineList()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Parmeter 2 is default val in minutes set at 1 minute here for test purposes.
        // The preference key *refresh_interval* is defined in *res/xml/settings.xml*.
        String refreshInterval = prefs.getString("refresh_interval", "1");
        // Precondition: user-input refresh frequency units are minutes.
        // Convert refreshFrequency minutes to milliseconds.
        int refreshFrequency = Integer.parseInt(refreshInterval) * 60 * 1000;
        int initialDelay = 1000; // Set an initial delay of 1 second

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startService(new Intent(getBaseContext(), RefreshService.class));
            }
        }, initialDelay, refreshFrequency);
    }
}


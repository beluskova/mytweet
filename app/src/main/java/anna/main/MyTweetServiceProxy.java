package anna.main;

import java.util.List;

import anna.models.Tweet;
import anna.models.Tweeter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MyTweetServiceProxy
{
    // Create a tweeter
    @POST("/api/tweeters")
    Call<Tweeter> createTweeter(@Body Tweeter tweeter);

    // Get all tweets
    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    // Create a tweet
    @POST("/api/tweeters/{id}/tweets")
    Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

    @POST("/api/tweets")
    Call<Tweet> createTweet(Long id, @Body Tweet tweet);

    @GET("/api/tweeters")
    Call<List<Tweeter>> getAllTweeters();

    @GET("/api/tweeters/{id}")
    Call<Tweeter> getTweeter(@Path("id") String id);

    @DELETE("/api/tweeters/{id}")
    Call<Tweeter> deleteTweeter(@Path("id") String id);

    @DELETE("/api/tweeters")
    Call<String> deleteAllTweeters();

    @DELETE("/api/tweets")
    Call<String> deleteAllTweets();

    @GET("/api/tweeters/{id}/tweets")
    Call<List<Tweet>> getTweets(@Path("id") String id);

    @GET("/api/tweeters/{id}/tweets/{tweetId}")
    Call<Tweet> getTweet(@Path("id") String id, @Path("tweetId") String tweetId);

    @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
    Call<Tweet> deleteTweet(@Path("id") String id, @Path("tweetId") String tweetId);
}
